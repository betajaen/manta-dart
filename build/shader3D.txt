precision mediump float;

attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec3 aColour;

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProjection;
uniform mat4 uNormal;
uniform vec3 uCamera;

varying vec3 vLighting;
varying vec3 vColour;

void main(void)
{
  gl_Position = uProjection * uView * uModel * vec4(aPosition, 1.0);
  gl_PointSize = 1.0;
  vColour = aColour;

  vec3 ambientLight = vec3(0.8, 0.8, 0.8);
  vec3 directionalLightColor = vec3(0.75, 0.75, 0.75);
  vec3 directionalVector = vec3(0.85, 0.8, 0.75);

  vec4 transformedNormal = uNormal * vec4(aNormal, 1.0);

  float directional = max(dot(transformedNormal.xyz, directionalVector), 0.0);
  vLighting = ambientLight + (directionalLightColor * directional);
}

---

precision mediump float;

varying vec3 vLighting;
varying vec3 vColour;
varying float vDistance;

uniform vec3 uTint;
uniform float uClipDistance;

void main(void)
{

  vec4 col = vec4(vColour, 1.0);

  // Shadowing, as black
  if (col.r == 0.0 && col.g == 0.0 && col.b == 0.0)
  {
    float h = mod(floor(gl_FragCoord.y),2.0);
    if(mod(floor(gl_FragCoord.x + h),2.0) > 0.5)
        discard;
    gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    return;
  }
    float z = (gl_FragCoord.z / gl_FragCoord.w) / 1000.0;
  if (uClipDistance < 1.0 && z > uClipDistance)
    discard;

  gl_FragColor = col * vec4(vLighting, 1.0) * vec4(uTint, 1.0);
}
