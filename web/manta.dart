/**

    Manta

    Copyright (c) 2013 Robin Southern, https://bitbucket.org/betajaen/manta

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

*/

library manta;

import 'dart:html';
import 'dart:web_gl' as GL;
import 'dart:math' as Math;
import 'dart:typed_data';
import 'package:vector_math/vector_math.dart';
import 'package:obj/obj.dart';

part 'intro.dart';
part 'flight.dart';
part 'colours.dart';
part 'aircraft.dart';
part 'world.dart';

///////////////////////////////////////////////

GL.RenderingContext gl;

///////////////////////////////////////////////

class CUtil
{

  static double approxRandom(Math.Random rng, double s, double t)
  {
    double u = 1.0 + (rng.nextDouble() * t) - t * 0.5;
    return s * u;
  }

  static double approxRandom01(Math.Random rng, double s, double t)
  {
    return clamp01(approxRandom(rng, s, t));
  }

  static double angle(Vector3 a, Vector3 b)
  {
    double n = a.length * b.length;
    if (n < 0.1)
    {
      return 0.0;
    }
    double t = clamp01(a.dot(b) / n);
    return Math.acos(t);
  }

  static double clamp(double value, double min, double max)
  {
    if (value > max)
      value = max;
    else if (value < min)
      value = min;
    return value;
  }

  static double clamp01(double value)
  {
    if (value > 1.0)
      value = 1.0;
    else if (value < 0.0)
      value = 0.0;
    return value;
  }

  static double clampn11(double value)
  {
    if (value > 1.0)
      value = 1.0;
    else if (value < -1.0)
      value = -1.0;
    return value;
  }

  static double toZero(double now, double d, double t)
  {
    if (now.abs() < 0.01)
      return 0.0;
    return now * (1.0 - (d * t));
  }
}


///////////////////////////////////////////////

abstract class CResource
{
  static const int kResource_Mesh = 0;
  static const int kResource_Shader = 1;
  static const int kResource_Image = 2;

  int    mType;
  String mURL;
  bool   mLoaded;

  void startLoading();
  bool hasLoaded()
  {
    return mLoaded;
  }
}

///////////////////////////////////////////////

class CImage extends CResource
{

  ImageElement  mImage;
  GL.Texture    mTexture;
  int           mWidth;
  int           mHeight;

  CImage(String url)
  {
    mType = CResource.kResource_Image;
    mURL = url;
    mLoaded = false;
    mWidth = 0;
    mHeight = 0;
  }

  void startLoading()
  {
    mImage = new ImageElement();
    mImage.onLoad.listen(handleImage);
    mImage.src = mURL;
  }

  void handleImage(e)
  {
    mTexture = gl.createTexture();
    gl.bindTexture(GL.TEXTURE_2D, mTexture);
    gl.texImage2DImage(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, mImage);
    gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);
    gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
    mWidth = mImage.width;
    mHeight = mImage.height;
    mLoaded = true;
    print("Loaded '$mURL' as '${mTexture}' ${mWidth}x${mHeight}px image");
  }
}

///////////////////////////////////////////////

class CBuffer
{
  GL.Buffer mVertices;
  GL.Buffer mNormals;
  GL.Buffer mColours;
  GL.Buffer mTextureCoords;
  GL.Buffer mIndexes;
  int       mNbVertices;
  int       mNbIndexes;
  int       mType;

  CBuffer()
  {
    mNbVertices = 0;
    mNbIndexes = 0;
    mType = GL.TRIANGLES;
  }

  void createVertices(List<double> vertices)
  {
    mNbVertices = vertices.length ~/ 3;
    mVertices = gl.createBuffer();
    gl.bindBuffer(GL.ARRAY_BUFFER, mVertices);
    gl.bufferDataTyped(GL.ARRAY_BUFFER, new Float32List.fromList(vertices), GL.STATIC_DRAW);
  }

  void createNormals(List<double> normals)
  {
    mNormals = gl.createBuffer();
    gl.bindBuffer(GL.ARRAY_BUFFER, mNormals);
    gl.bufferDataTyped(GL.ARRAY_BUFFER, new Float32List.fromList(normals), GL.STATIC_DRAW);
  }

  void createColours(List<double> colours)
  {
    mColours = gl.createBuffer();
    gl.bindBuffer(GL.ARRAY_BUFFER, mColours);
    gl.bufferDataTyped(GL.ARRAY_BUFFER, new Float32List.fromList(colours), GL.STATIC_DRAW);
  }

  void createTextureCoords(List<double> textureCoords)
  {
    mTextureCoords = gl.createBuffer();
    gl.bindBuffer(GL.ARRAY_BUFFER, mTextureCoords);
    gl.bufferDataTyped(GL.ARRAY_BUFFER, new Float32List.fromList(textureCoords), GL.STATIC_DRAW);
  }

  void createIndexes(List<int> indexes, int type)
  {
    mType = type;
    mIndexes = gl.createBuffer();
    gl.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, mIndexes);
    gl.bufferDataTyped(GL.ELEMENT_ARRAY_BUFFER, new Uint16List.fromList(indexes), GL.STATIC_DRAW);
    mNbIndexes = indexes.length;
  }

}


///////////////////////////////////////////////

class CMesh extends CResource
{

  Obj           mObj;
  CBuffer       mBuffer;

  CMesh(String url)
  {
    mType = CResource.kResource_Mesh;
    mURL = url;
    mLoaded = false;

  }

  void startLoading()
  {
    if (mURL.length != 0)
    {
      HttpRequest.getString(mURL)
      .then(handleObj)
      .catchError((err) { print("Failure fetching OBJ from URL: $mURL: $err"); });
    }
    else
    {
      mLoaded = true;
    }
  }

  void handleObj(String objString)
  {
    mObj = new Obj.fromString(mURL, objString);
    mBuffer = new CBuffer();
    mBuffer.createVertices(mObj.vertCoord);
    mBuffer.createNormals(mObj.normCoord);
    mBuffer.createIndexes(mObj.indices, GL.TRIANGLES);

    List<double> colours = new List<double>(mObj.vertCoord.length);

    mObj.partList.forEach((pa) {
      Vector3 colour = Colours.get(pa.name);
      int begin = pa.indexFirst;
      int end = begin + pa.indexListSize;
      for(int i=begin;i < end;i++)
      {
        int index = mObj.indices[i];
        colours[index * 3 + 0] = colour.x;
        colours[index * 3 + 1] = colour.y;
        colours[index * 3 + 2] = colour.z;
      }
    });

    mBuffer.createColours(colours);

    print("Loaded '$mURL' as ${mObj.vertCoord.length} verts/${mObj.indices.length} tri mesh");
    mLoaded = true;
  }
}

///////////////////////////////////////////////

class CTransform
{

  static const sYOffsetRange = 10.0;

  Matrix4 mModel, mView, mProjection, mTexture, mNormal;

  Vector3 mCameraUp, mCameraPosition, mCameraTarget;

  List<Matrix4> mModelStack;

  double mYOffset;

  Vector3 mTint;

  double mClipDistance;

  CTransform()
  {
    mModel = new Matrix4.identity();
    mProjection = new Matrix4.identity();
    mView = new Matrix4.identity();
    mTexture = new Matrix4.identity();

    mCameraUp = new Vector3(0.0, 1.0, 0.0);
    mCameraPosition = new Vector3(0.0, 0.0, 0.0);
    mCameraTarget = new Vector3(0.0, 0.0, -1.0);

    mModelStack = new List<Matrix4>();
    mModelStack.add(mModel.clone());

    mTint = new Vector3(1.0, 1.0, 1.0);

    mYOffset = 0.0;

    mClipDistance = 1.0;

    updateCamera();
  }

  void push(Vector3 position, Vector3 rotation, Vector3 scale, Vector3 tint, double clipDistance)
  {
    mModelStack.add(mModel.clone());
    mModel.scale(scale);
    mModel.translate(position.x, position.y + mYOffset * sYOffsetRange, position.z);
    mModel.rotateX(rotation.x * degrees2radians);
    mModel.rotateY(rotation.y * degrees2radians);
    mModel.rotateZ(rotation.z * degrees2radians);
    mTint.setFrom(tint);
    mClipDistance = clipDistance;
    updateNormal();
  }

  void pop()
  {
    if (mModelStack.length > 1)
    {
      mModel.setFrom(mModelStack.removeLast());
    }
  }

  void updateCamera()
  {
    setViewMatrix(mView, mCameraPosition, mCameraTarget, mCameraUp);
  }

  void updateNormal()
  {
    mNormal = mView * mModel;
    mNormal.invert();
    mNormal.transpose();
  }

  void setCameraLookAt(Vector3 cameraPos, Vector3 cameraTarget)
  {
    mCameraPosition.setFrom(cameraPos);
    mCameraTarget.setFrom(cameraTarget);
    updateCamera();
  }

  void setCameraOrbitCosSin(Vector3 origin, double angleDegrees, double radius, double yOffset)
  {
    mCameraPosition.x = origin.x + Math.cos(angleDegrees * degrees2radians) * radius;
    mCameraPosition.y = origin.y + yOffset;
    mCameraPosition.z = origin.z + Math.sin(angleDegrees * degrees2radians) * radius;
    mCameraTarget.setFrom(origin);
    updateCamera();
  }

  void setProjection2D()
  {
    mProjection = makeOrthographicMatrix(0, 320.0, 200.0, 0, 0, 10);
  }

  void setProjection3DTerrain()
  {
    mProjection = makePerspectiveMatrix(60.0 * degrees2radians, 320.0 / 200.0, 10, 10000);
  }

  void setProjection3D()
  {
    mProjection = makePerspectiveMatrix(60.0 * degrees2radians, 320.0 / 200.0, 0.1, 1000);
  }
}

///////////////////////////////////////////////

class CProgram
{

  static const int kUniform_Matrix4_Model = 0;
  static const int kUniform_Matrix4_View = 1;
  static const int kUniform_Matrix4_Projection = 2;
  static const int kUniform_Matrix4_Texture = 3;
  static const int kUniform_Sampler_Image = 4;
  static const int kUniform_Vec3_Tint = 5;
  static const int kUniform_Vec3_Camera = 6;
  static const int kUniform_float_ClipDistance = 7;
  static const int kUniform_COUNT = 8;

  static List<String> kUniform_NAMES = ['uModel', 'uView', 'uProjection', 'uTexture', 'uImage', 'uTint', 'uCamera', 'uClipDistance'];

  static const int kAttribute_Vec3_Position = 0;
  static const int kAttribute_Vec3_Normal = 1;
  static const int kAttribute_Vec2_TextureCoord = 2;
  static const int kAttribute_Vec3_Colour = 3;
  static const int kAttribute_COUNT = 4;

  static List<String> kAttribute_NAMES = ['aPosition', 'aNormal', 'aTextureCoords', 'aColour'];

  List<GL.UniformLocation> mUniforms;
  List<int> mAttributes;

  GL.Program mProgram;
  GL.Shader  mVertexShader, mFragmentShader;
  bool       mIsCompiled;

  CProgram(String vertexShaderSrc, String fragmentShaderSrc)
  {
    mIsCompiled = false;
    mUniforms = new List<GL.UniformLocation>(kUniform_COUNT);
    mAttributes = new List<int>(kAttribute_COUNT);

    mVertexShader = gl.createShader(GL.VERTEX_SHADER);
    gl.shaderSource(mVertexShader, vertexShaderSrc);
    gl.compileShader(mVertexShader);

    mFragmentShader = gl.createShader(GL.FRAGMENT_SHADER);
    gl.shaderSource(mFragmentShader, fragmentShaderSrc);
    gl.compileShader(mFragmentShader);

    mProgram = gl.createProgram();
    gl.attachShader(mProgram, mVertexShader);
    gl.attachShader(mProgram, mFragmentShader);
    gl.linkProgram(mProgram);

    mIsCompiled = true;

    if (!gl.getShaderParameter(mVertexShader, GL.COMPILE_STATUS))
    {
      print(gl.getShaderInfoLog(mVertexShader));
      mIsCompiled = false;
    }

    if (!gl.getShaderParameter(mFragmentShader, GL.COMPILE_STATUS))
    {
      print(gl.getShaderInfoLog(mFragmentShader));
      mIsCompiled = false;
    }

    if (!gl.getProgramParameter(mProgram, GL.LINK_STATUS))
    {
      print(gl.getProgramInfoLog(mProgram));
      mIsCompiled = false;
    }

    if (mIsCompiled)
    {
      discover();
    }
  }

  void discover()
  {
    gl.useProgram(mProgram);

    for(int i=0;i < CProgram.kUniform_COUNT;i++)
    {
      mUniforms[i] = gl.getUniformLocation(mProgram, CProgram.kUniform_NAMES[i]);
    }

    for(int i=0;i < CProgram.kAttribute_COUNT;i++)
    {
      mAttributes[i] = gl.getAttribLocation(mProgram, CProgram.kAttribute_NAMES[i]);
      if (mAttributes[i] != -1)
      {
        gl.enableVertexAttribArray(mAttributes[i]);
      }
    }

    gl.useProgram(null);
  }

  void draw(CTransform transform, CBuffer buffer, GL.Texture image)
  {
    if (mIsCompiled)
    {
      gl.useProgram(mProgram);

      // Image is optional
      if (image != null)
      {
        gl.activeTexture(GL.TEXTURE0);
        gl.bindTexture(GL.TEXTURE_2D, image);
        gl.uniform1i(mUniforms[CProgram.kUniform_Sampler_Image], 0);
      }

      // Model is optional
      if (mUniforms[CProgram.kUniform_Matrix4_Model] != null)
      {
        gl.uniformMatrix4fv(mUniforms[CProgram.kUniform_Matrix4_Model], false, transform.mModel.storage);
      }

      // View is optional
      if (mUniforms[CProgram.kUniform_Matrix4_View] != null)
      {
        gl.uniformMatrix4fv(mUniforms[CProgram.kUniform_Matrix4_View], false, transform.mView.storage);
      }

      // Projection is optional.
      if (mUniforms[CProgram.kUniform_Matrix4_Projection] != null)
      {
        gl.uniformMatrix4fv(mUniforms[CProgram.kUniform_Matrix4_Projection], false, transform.mProjection.storage);
      }

      // Texture is optional
      if (mUniforms[CProgram.kUniform_Matrix4_Texture] != null)
      {
        gl.uniformMatrix4fv(mUniforms[CProgram.kUniform_Matrix4_Texture], false, transform.mTexture.storage);
      }

      // Tint is optional
      if (mUniforms[CProgram.kUniform_Vec3_Tint] != null)
      {
        gl.uniform3fv(mUniforms[CProgram.kUniform_Vec3_Tint], transform.mTint.storage);
      }

      // Camera is optional
      if (mUniforms[CProgram.kUniform_Vec3_Camera] != null)
      {
        gl.uniform3fv(mUniforms[CProgram.kUniform_Vec3_Camera], transform.mCameraPosition.storage);
      }

      // Clip distnce is optional
      if (mUniforms[CProgram.kUniform_float_ClipDistance] != null)
      {
        gl.uniform1f(mUniforms[CProgram.kUniform_float_ClipDistance], transform.mClipDistance);
      }

      // Vertices are mandatory
      gl.bindBuffer(GL.ARRAY_BUFFER, buffer.mVertices);
      gl.vertexAttribPointer(mAttributes[CProgram.kAttribute_Vec3_Position], 3, GL.FLOAT, false, 0, 0);

      // Normals are optional
      if (buffer.mNormals != null)
      {
        gl.bindBuffer(GL.ARRAY_BUFFER, buffer.mNormals);
        gl.vertexAttribPointer(mAttributes[CProgram.kAttribute_Vec3_Normal], 3, GL.FLOAT, false, 0, 0);
      }

      // Texture coords are optional
      if (buffer.mTextureCoords != null)
      {
        gl.bindBuffer(GL.ARRAY_BUFFER, buffer.mTextureCoords);
        gl.vertexAttribPointer(mAttributes[CProgram.kAttribute_Vec2_TextureCoord], 2, GL.FLOAT, false, 0, 0);
      }

      // Colours are optional
      if (buffer.mColours != null)
      {
        gl.bindBuffer(GL.ARRAY_BUFFER, buffer.mColours);
        gl.vertexAttribPointer(mAttributes[CProgram.kAttribute_Vec3_Colour], 3, GL.FLOAT, false, 0, 0);
      }

      gl.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, buffer.mIndexes);
      gl.drawElements(buffer.mType, buffer.mNbIndexes, GL.UNSIGNED_SHORT, 0);

      gl.useProgram(null);
    }
  }
}

///////////////////////////////////////////////

class CShader extends CResource
{
  CProgram  mProgram;
  String    mVertexSource;
  String    mFragmentSource;

  CShader(String url)
  {
    mURL = url;
    mLoaded = false;
  }

  void startLoading()
  {
    HttpRequest.getString(mURL)
    .then(handleShader)
    .catchError((err) { print("Failure fetching OBJ from URL: $mURL: $err"); });
  }

  void handleShader(String objString)
  {
    var split = objString.split('---');
    mVertexSource = split[0].trim();
    mFragmentSource = split[1].trim();
    mProgram = new CProgram(mVertexSource, mFragmentSource);
    mLoaded = true;
    print("Loaded '$mURL' compiled=${mProgram.mIsCompiled} as shader");
  }

  void draw(CTransform transform, CBuffer buffer, GL.Texture image)
  {
    if (mProgram != null)
    {
      mProgram.draw(transform, buffer, image);
    }
    else
    {
      print("Not drawing through this shader!");
    }
  }
}

///////////////////////////////////////////////

class Art
{
  static CImage   ui;
  static CMesh    plane, logo, runway, hanger, refdot, trees, forest;
  static CShader  shader3D, shader2D, shaderRTT;
}

///////////////////////////////////////////////

class CGame
{

  List<CResource> mResources;
  List<CResource> mLoadingResources;
  List<CState>    mStateStack;

  double          mLastTime = 0.0;
  double          mDeltaTime = 0.0;

  Math.Random     mRNG;

  CanvasElement   mCanvas;
  int             mWidth = 320;
  int             mHeight = 200;
  int             mTargetWidth;
  int             mTargetHeight;
  int             mTargetScale = 3;
  bool            mScaling;
  CBuffer         mScalingBuffer;
  GL.Texture      mScalingTexture;
  GL.Framebuffer  mScalingFrameBuffer;
  GL.Renderbuffer mScalingRenderBuffer;
  int             mScalingTextureWidth;
  int             mScalingTextureHeight;
  CTransform      mScalingTransform;

  List<bool>      mKeyDown;
  List<bool>      mKeyWasDown;

  CGame()
  {
    Game = this;
    mRNG = new Math.Random();
    mStateStack = new List<CState>();
    mResources = new List<CResource>();
    mLoadingResources = new List<CResource>();

    mCanvas = querySelector("#game");
    mTargetWidth = mWidth * mTargetScale;
    mTargetHeight = mHeight * mTargetScale;
    mCanvas.width = mTargetWidth;
    mCanvas.height = mTargetHeight;

    mScalingTextureWidth = 256;
    mScalingTextureHeight = 256;
    while(mTargetWidth > mScalingTextureWidth)
    {
      mScalingTextureWidth *= 2;
    }

    mScalingTextureHeight = mScalingTextureWidth;

    mScaling = false;

    gl = mCanvas.getContext3d(antialias: false);

    if (gl == null)
    {
      print("Bad GL");
      return;
    }

    pushState(new CLoadingState());
  }

  bool hasResourcesLoaded()
  {
    return mLoadingResources.length == 0;
  }

  void enableScaling()
  {
    print("Enabling scaling 320x200 to ${mTargetWidth}x${mTargetHeight} through ${mScalingTextureWidth}x${mScalingTextureHeight} texture");

    mScaling = true;

    mScalingTransform = new CTransform();

    mScalingBuffer = new CBuffer();
    mScalingBuffer.createVertices([
            -1.0, -1.0, 0.0,
            -1.0,  1.0, 0.0,
             1.0,  1.0, 0.0,
             1.0, -1.0, 0.0,
        ]);

    double mw = 320.0 / mScalingTextureWidth.toDouble();
    double mh = 200.0 / mScalingTextureHeight.toDouble();

    mScalingBuffer.createTextureCoords([
        0.0,  0.0,
        0.0,  mh,
        mw,   mh,
        mw,   0.0,
    ]);

    mScalingBuffer.createIndexes([
        0, 1, 2,
        0, 2, 3
    ], GL.TRIANGLES);

    mScalingFrameBuffer = gl.createFramebuffer();
    gl.bindFramebuffer(GL.FRAMEBUFFER, mScalingFrameBuffer);

    mScalingTexture = gl.createTexture();
    gl.bindTexture(GL.TEXTURE_2D, mScalingTexture);
    gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);
    gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
    gl.texImage2DTyped(GL.TEXTURE_2D, 0, GL.RGBA, mScalingTextureWidth, mScalingTextureHeight, 0, GL.RGBA, GL.UNSIGNED_BYTE, null);
    gl.generateMipmap(GL.TEXTURE_2D);

    mScalingRenderBuffer = gl.createRenderbuffer();
    gl.bindRenderbuffer(GL.RENDERBUFFER, mScalingRenderBuffer);
    gl.renderbufferStorage(GL.RENDERBUFFER, GL.DEPTH_COMPONENT16, mScalingTextureWidth, mScalingTextureHeight);

    gl.framebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, mScalingTexture, 0);
    gl.framebufferRenderbuffer(GL.FRAMEBUFFER, GL.DEPTH_ATTACHMENT, GL.RENDERBUFFER, mScalingRenderBuffer);

    gl.bindTexture(GL.TEXTURE_2D, null);
    gl.bindRenderbuffer(GL.RENDERBUFFER, null);
    gl.bindFramebuffer(GL.FRAMEBUFFER, null);
  }

  void enableGUI()
  {
    GUI = new CGUI();
  }

  void enableInput()
  {
    mKeyDown = new List<bool>(255);
    mKeyWasDown = new List<bool>(255);
    for(int i=0;i < 255;i++)
    {
      mKeyWasDown[i] = false;
      mKeyDown[i] = false;
    }
    window.onKeyUp.listen(onKeyUp);
    window.onKeyDown.listen(onKeyDown);
  }

  void onKeyDown(KeyboardEvent e)
  {
    if (e.keyCode < 255)
    {
      mKeyDown[e.keyCode] = true;
    }
  }

  void onKeyUp(KeyboardEvent e)
  {
    if (e.keyCode < 255)
    {
      mKeyDown[e.keyCode] = false;
    }
  }

  bool isKeyDown(int keyCode)
  {
    return mKeyDown[keyCode];
  }

  bool isKeyReleased(int keyCode)
  {
    return mKeyDown[keyCode] == false && mKeyWasDown[keyCode] == true;
  }

  CImage loadImage(String url)
  {
    CImage image = new CImage(url);
    mResources.add(image);
    mLoadingResources.add(image);
    image.startLoading();
    return image;
  }

  CMesh loadMesh(String url)
  {
    CMesh mesh = new CMesh(url);
    mResources.add(mesh);
    mLoadingResources.add(mesh);
    mesh.startLoading();
    return mesh;
  }

  CShader loadShader(String url)
  {
    CShader shader = new CShader(url);
    mResources.add(shader);
    mLoadingResources.add(shader);
    shader.startLoading();
    return shader;
  }

  void tick(double time)
  {

    mDeltaTime = (time - mLastTime).toDouble() * 0.001;
    mLastTime = time;

    bool isScaling = false;

    if (mScaling)
    {
      isScaling = true;
      gl.viewport(0, 0, 320, 200);
      gl.bindFramebuffer(GL.FRAMEBUFFER, mScalingFrameBuffer);
    }
    else
    {
      gl.viewport(0, 0, mTargetWidth, mTargetHeight);
    }

    gl.clearColor(0.4, 0.49, 0.7337, 1.0);
    gl.clearDepth(1.0);
    gl.enable(GL.DEPTH_TEST);
    //gl.depthFunc(GL.LEQUAL);
    gl.depthFunc(GL.LESS);

    if (mStateStack.length != 0)
    {
      mStateStack.last.tick(mDeltaTime);
    }

    if (isScaling)
    {
      gl.bindTexture(GL.TEXTURE_2D, mScalingTexture);
      gl.generateMipmap(GL.TEXTURE_2D);
      gl.bindTexture(GL.TEXTURE_2D, null);
      gl.bindFramebuffer(GL.FRAMEBUFFER, null);

      gl.viewport(0, 0, mTargetWidth, mTargetHeight);
      Art.shaderRTT.draw(mScalingTransform, mScalingBuffer, mScalingTexture);
    }

    if (mKeyWasDown != null)
    {
      for(int i=0;i < 255;i++)
      {
        mKeyWasDown[i] = mKeyDown[i];
        // mKeyDown[i] = false;
      }
    }

    for(CResource resource in mLoadingResources)
    {
      if (resource.hasLoaded())
      {
        mLoadingResources.remove(resource);
        print("Loaded " + resource.mURL);
        break;
      }
    }

  }

  void pushState(CState state)
  {
    mStateStack.add(state);
  }

  CState popState()
  {
    CState state = mStateStack.removeLast();
    state.stop();
    return state;
  }

  void swapState(CState state)
  {
    popState();
    pushState(state);
  }

}

CGame Game;

///////////////////////////////////////////////

abstract class CState
{
  void stop();
  void tick(double deltaTime);
}

///////////////////////////////////////////////

class CLoadingState extends CState
{

  CLoadingState()
  {
    print("Loading resources");
    Art.ui = Game.loadImage('t.png');
    Art.plane = Game.loadMesh('plane.txt');
    Art.logo = Game.loadMesh('logo.txt');
    Art.hanger = Game.loadMesh('hanger.txt');
    Art.runway = Game.loadMesh('runway.txt');
    Art.refdot = createRefDot();
    Art.trees = Game.loadMesh('trees.txt');
    Art.forest = Game.loadMesh('forest.txt');
    Art.shader2D = Game.loadShader('shader2D.txt');
    Art.shader3D = Game.loadShader('shader3D.txt');
    Art.shaderRTT = Game.loadShader('shaderRTT.txt');
  }

  void stop()
  {
  }

  void tick(double deltaTime)
  {
    gl.clearColor(Game.mRNG.nextDouble() * 0.1, Game.mRNG.nextDouble() * 0.1, Game.mRNG.nextDouble() * 0.1, 1.0);
    gl.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

    if (Game.hasResourcesLoaded())
    {
      Game.enableScaling();
      Game.enableGUI();
      Game.enableInput();
      Game.swapState(new CIntroState());
      return;
    }
  }

  CMesh createRefDot()
  {
    Math.Random rng = new Math.Random(3929);
    int nbPoints = 24;
    CMesh mesh = Game.loadMesh("");
    mesh.mBuffer = new CBuffer();
    List<double> vertices = new List<double>(nbPoints * 3);
    List<double> colours = new List<double>(nbPoints * 3);
    List<double> normals = new List<double>(nbPoints * 3);
    List<int> indexes = new List<int>(nbPoints);
    int k=0;
    for(int i=0;i < nbPoints;i++)
    {
      double x = rng.nextDouble() * 256.0 - 128.0;
      double z = rng.nextDouble() * 256.0 - 128.0;
      vertices[k+0] = x;
      vertices[k+1] = 0.1;
      vertices[k+2] = z;
      colours[k+0] = 1.0;
      colours[k+1] = 1.0;
      colours[k+2] = 1.0;
      normals[k+0] = 0.0;
      normals[k+1] = 1.0;
      normals[k+2] = 0.0;
      indexes[i] = i;
      k+=3;
    }
    mesh.mBuffer.createVertices(vertices);
    mesh.mBuffer.createNormals(normals);
    mesh.mBuffer.createColours(colours);
    mesh.mBuffer.createIndexes(indexes, GL.POINTS);
    return mesh;
  }

}

///////////////////////////////////////////////

class CNode
{

  static const  int kObjects = 0x01;
  static const  int kTerrain = 0x02;
  static const  int kAll = kObjects | kTerrain;

  CTransform    mTransform;

  CMesh         mRenderable, mRenderableShadow;
  Vector3       mPosition;
  Vector3       mRotation;
  Vector3       mScale;
  Vector3       mTint;
  List<CNode>   mChildren;
  bool          mVisible;
  int           mMask = kObjects;
  double        mClipDistance;

  CNode()
  {
    mClipDistance = 1.0;
    mPosition = new Vector3(0.0, 0.0, 0.0);
    mRotation = new Vector3(0.0, 0.0, 0.0);
    mScale = new Vector3(1.0, 1.0, 1.0);
    mTint = new Vector3(1.0, 1.0, 1.0);
    mVisible = true;
  }

  void setForRoot(CTransform transform)
  {
    mTransform = transform;
  }

  CNode addChild()
  {
    return addUserChild(new CNode());
  }

  CNode addUserChild(CNode userNode)
  {
    userNode.mTransform = mTransform;
    if (mChildren == null)
    {
      mChildren = new List<CNode>();
    }
    mChildren.add(userNode);
    return userNode;
  }

  void draw()
  {
    drawSelf(kAll);
  }

  void drawSelf(int mask)
  {
    if (mVisible == false)
      return;

    mTransform.push(mPosition, mRotation, mScale, mTint, mClipDistance);

    if (mRenderable != null && mMask & mask != 0)
    {
      Art.shader3D.draw(mTransform, mRenderable.mBuffer, null);
    }

    if (mChildren != null)
    {
      for(CNode child in mChildren)
      {
        child.drawSelf(mask);
      }
    }

    mTransform.pop();
  }


}

///////////////////////////////////////////////

class CGUI
{

  CTransform  mTransform;
  CBuffer     mBuffer;

  CGUI()
  {
    mTransform = new CTransform();
    mTransform.setProjection2D();

    mBuffer = new CBuffer();
    mBuffer.createVertices([
      -0.5, -0.5, 0.0,
       0.5, -0.5, 0.0,
       0.5,  0.5, 0.0,
      -0.5,  0.5, 0.0
    ]);
    mBuffer.createTextureCoords([
       0.0,  0.0,
       1.0,  0.0,
       1.0,  1.0,
       0.0,  1.0
    ]);
    mBuffer.createIndexes([
      0, 1, 2,
      0, 2, 3
    ], GL.TRIANGLES);
  }

  void draw(int x, int y, int z, int w, int h, int sourceX, int sourceY)
  {
    double mx = x.toDouble();
    double my = y.toDouble();
    double mw = w.toDouble();
    double mh = h.toDouble();
    double msx = sourceX.toDouble();
    double msy = sourceY.toDouble();

    mTransform.mModel.setIdentity();
    mTransform.mModel.translate( mx + mw / 2, my + mh / 2, -2.0);
    mTransform.mModel.scale(mw, mh, 0.0);

    mTransform.mTexture.setIdentity();
    mTransform.mTexture.scale(1.0 / 256.0, 1.0 / 256.0, 0.0);
    mTransform.mTexture.translate(msx, msy, 0.0);
    mTransform.mTexture.scale(mw, mh, 0.0);

    Art.shader2D.draw(mTransform, mBuffer, Art.ui.mTexture);
  }

  void drawStretch(int x, int y, int z, int w, int h, int sourceX, int sourceY, int sourceW, int sourceH)
  {
    double mx = x.toDouble();
    double my = y.toDouble();
    double mw = w.toDouble();
    double mh = h.toDouble();
    double msx = sourceX.toDouble();
    double msy = sourceY.toDouble();
    double msw = sourceW.toDouble();
    double msh = sourceH.toDouble();

    mTransform.mModel.setIdentity();
    mTransform.mModel.translate( mx + mw / 2, my + mh / 2, -2.0);
    mTransform.mModel.scale(mw, mh, 0.0);

    mTransform.mTexture.setIdentity();
    mTransform.mTexture.scale(1.0 / 256.0, 1.0 / 256.0, 0.0);
    mTransform.mTexture.translate(msx, msy, 0.0);
    mTransform.mTexture.scale(msw, msh, 0.0);

    Art.shader2D.draw(mTransform, mBuffer, Art.ui.mTexture);
  }

  void drawText(int x, int y, int z, String text)
  {
    for(int i=0; i < text.length; i++)
    {
      int cp = text.codeUnitAt(i) - 32;
      if (cp > 0 && cp <= 96)
      {
        draw(x, y, z, 6, 8, (cp % 32) * 6, (cp ~/ 32) * 8);
      }
      x += 6;
    }
  }

  int drawSmallText(int x, int y, int z, String text)
  {
    int ox = x;
    for(int i=0; i < text.length; i++)
    {
      int cp = text.codeUnitAt(i) - 32;
      if (cp == 14)
        x -= 1;
      if (cp > 0 && cp <= 96)
      {
        draw(x, y, z, 4, 5, (cp % 32) * 4, 25 + (cp ~/ 32) * 5);
      }
      if (cp == 14) // .
        x += 3;
      else if (cp >= 33 && cp <= 58)  // A-Z
        x += 5;
      else
        x += 4;
    }
    return x - ox;
  }

  int drawSmallTextAngle(int x, int y, int z, int angle)
  {

    String s;

    if (angle < 10)
    {
      s = "00${angle}";
    }
    else if (angle < 100)
    {
      s = "0${angle}";
    }
    else
    {
      s = "${angle}";
    }

    return drawSmallText(x, y, z, s);
  }

  int drawSmallTextAltitude(int x, int y, int z, int feet)
  {

    String s;

    //  009
    //  099
    //  999
    //  09K
    //  99K

    if (feet < 10)
    {
      s = "00${feet}";
    }
    else if (feet < 100)
    {
      s = "0${feet}";
    }
    else if (feet < 1000)
    {
      s = "${feet}";
    }
    else if (feet < 10000)
    {
      s = "0${feet ~/ 1000}K";
    }
    else
    {
      s = "${feet ~/ 1000}K";
    }


    return drawSmallText(x, y, z, s);
  }


  int drawSmallTextMPH(int x, int y, int z, int mph)
  {

    String s;

//  009
//  099
//  999

    if (mph < 10)
    {
      s = "00${mph}";
    }
    else if (mph < 100)
    {
      s = "0${mph}";
    }
    else if (mph < 1000)
    {
      s = "${mph}";
    }

    return drawSmallText(x, y, z, s);
  }

  void drawSurroundText(int x, int y, int z, String text)
  {
    int l = drawSmallText(x, y, z, text);

    draw(x-2, y-2, z, 5, 9, 128, 25);
    drawStretch(x, y-2, z, l - 1, 9, 129, 25, 5, 9);
    draw(x + l - 1, y-2, z, 2, 9, 133, 25);
  }

  void drawSurroundTextAngle(int x, int y, int z, int angle)
  {
    int l = drawSmallTextAngle(x, y, z, angle);

    draw(x-2, y-2, z, 5, 9, 128, 25);
    drawStretch(x, y-2, z, l - 1, 9, 129, 25, 5, 9);
    draw(x + l - 1, y-2, z, 2, 9, 133, 25);
  }

  void drawSurroundTextAltitude(int x, int y, int z, int feet)
  {
    int l = drawSmallTextAltitude(x, y, z, feet);

    draw(x-2, y-2, z, 5, 9, 128, 25);
    drawStretch(x, y-2, z, l - 1, 9, 129, 25, 5, 9);
    draw(x + l - 1, y-2, z, 2, 9, 133, 25);
  }

  void drawSurroundTextMPH(int x, int y, int z, int mph)
  {
    int l = drawSmallTextMPH(x, y, z, mph);

    draw(x-2, y-2, z, 5, 9, 128, 25);
    drawStretch(x, y-2, z, l - 1, 9, 129, 25, 5, 9);
    draw(x + l - 1, y-2, z, 2, 9, 133, 25);
  }
}

CGUI GUI;

///////////////////////////////////////////////

void main()
{
  print("-=-=-=-=-=-=-=-=-");
  new CGame();
  window.requestAnimationFrame(tick);
}

///////////////////////////////////////////////

void tick(double time)
{
  if (gl != null)
  {
    window.requestAnimationFrame(tick);
    Game.tick(time);
  }
}

///////////////////////////////////////////////
