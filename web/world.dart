/**

    Manta

    Copyright (c) 2013 Robin Southern, https://bitbucket.org/betajaen/manta

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

*/

part of manta;

///////////////////////////////////////////////

class CWorld
{

  CNode mRootNode;

  int mPlayerX, mPlayerZ;
  int mRadius;
  CChunk mPlayerChunk;
  List<CChunk> mChunks;
  bool mPlayerMoved;
  double mCleanTime;

  List<CChunkGenerator> mGenerators;

  CWorld(CTransform transform)
  {
    world = this;
    mPlayerX = 0;
    mPlayerZ = 0;
    mGenerators = new List<CChunkGenerator>();
    mGenerators.add(new CChunkGeneratorAirport());
    mGenerators.add(new CChunkGeneratorTerrain());
    mGenerators.add(new CChunkGeneratorUrban());
    mGenerators.add(new CChunkGeneratorWater());
    mChunks = new List<CChunk>();
    mRootNode = new CNode();
    mRootNode.setForRoot(transform);
    mPlayerMoved = false;
    mRadius = 8;
    mCleanTime = 0.0;

  }

  void tick(double dt)
  {
    if (mPlayerMoved)
    {
      mPlayerMoved = false;
      mPlayerChunk = Get(mPlayerX, mPlayerZ);
      mPlayerChunk.setEnabled(true);
      for(CChunk chunk in mChunks)
      {
        chunk.setEnabled(false);
      }
      for(int x = mPlayerX - mRadius;x < mPlayerX + mRadius;x++)
      {
        for(int z = mPlayerZ - mRadius;z < mPlayerZ + mRadius;z++)
        {
          CChunk chunk = Get(x, z);
          chunk.setEnabled(true);
          chunk.setEnabled(true);
        }
      }
    }
    if (mCleanTime > 5.0)
    {
      mCleanTime = 0.0;

      while(true)
      {
        bool didSomething = false;
        for(CChunk chunk in mChunks)
        {
          if (chunk.distance(mPlayerChunk) > mRadius + 2)
          {
            mChunks.remove(chunk);
            didSomething = true;
            break;
          }
        }
        if (didSomething)
          continue;
        break;
      }
    }
    mCleanTime += dt;
  }

  void SetPlayer(Vector3 position)
  {
    int px = (position.x / CChunk.sSize).round();
    int pz = (position.z / CChunk.sSize).round();
    if (px != mPlayerX || pz != mPlayerZ)
    {
      mPlayerX = px;
      mPlayerZ = pz;
      mPlayerMoved = true;
    }
  }

  CChunk Get(int x, int z)
  {
    for(CChunk chunk in mChunks)
    {
      if (chunk != null && chunk.mX == x && chunk.mZ == z)
      {
        return chunk;
      }
    }

    CChunk chunk = new CChunk(x, z);
    mChunks.add(chunk);
    return chunk;
  }

}

CWorld world;

///////////////////////////////////////////////

abstract class CChunkGenerator
{
  void generate(CChunk chunk);

  void tintPlane(CChunk chunk, String colourName, double r, double g, double b)
  {

    Vector3 colour = Colours.get(colourName);
    colour.r = CUtil.approxRandom01(chunk.mRNG, colour.r, 0.1);
    colour.g = CUtil.approxRandom01(chunk.mRNG, colour.g, 0.1);
    colour.b = CUtil.approxRandom01(chunk.mRNG, colour.b, 0.1);

    chunk.mPlane.mTint.setFrom(colour);
    chunk.mDetail.mTint.setFrom(colour * 0.65);
  }
}

///////////////////////////////////////////////

class CChunkGeneratorAirport extends CChunkGenerator
{
  void generate(CChunk chunk)
  {
    tintPlane(chunk, "Gray", 0.1, 0.1, 0.1);
    tintPlane(chunk, "Gray", 0.1, 0.1, 0.1);
  }
}

///////////////////////////////////////////////

class CChunkGeneratorTerrain extends CChunkGenerator
{
  void generate(CChunk chunk)
  {
    tintPlane(chunk, "LimeGreen", 0.05, 0.6, 0.2);
    int nbTrees = chunk.mRNG.nextInt(2);
    for (int i=0;i < nbTrees;i++)
    {
      double x = chunk.mRNG.nextDouble() * 256.0 - 128.0;
      double z = chunk.mRNG.nextDouble() * 256.0 - 128.0;
      CNode trees = chunk.mNode.addChild();
      trees.mPosition.setValues(x, 0.0, z);
      trees.mRotation.y = chunk.mRNG.nextDouble();
      trees.mRenderable = Art.forest;
    }
  }
}

///////////////////////////////////////////////

class CChunkGeneratorUrban extends CChunkGenerator
{
  void generate(CChunk chunk)
  {
    tintPlane(chunk, "LightGray", 0.05, 0.6, 0.2);
  }
}

///////////////////////////////////////////////

class CChunkGeneratorWater extends CChunkGenerator
{
  void generate(CChunk chunk)
  {
    tintPlane(chunk, "Blue", 0.01, 0.01, 0.4);
  }
}
///////////////////////////////////////////////

class CChunk
{
  static const double sSize = 256.0;

  int mX, mZ, mBiomeX, mBiomeZ, mBiomeType;
  bool mEnabled;
  CNode mNode, mPlane, mDetail;
  Math.Random mRNG;

  CChunk(int x, int z)
  {
    mEnabled = false;
    mX = x;
    mZ = z;
    mBiomeX = (mX.toDouble() / 2.0).round();
    mBiomeZ = (mZ.toDouble() / 2.0).round();

    mNode = world.mRootNode.addChild();
    mNode.mVisible = false;
    mNode.mPosition.setValues(x.toDouble() * sSize, 0.0,z.toDouble() * sSize);

    mPlane = mNode.addChild();
    mPlane.mRenderable = Art.plane;
    mPlane.mMask = CNode.kTerrain;
    /*
    */
    mDetail = mNode.addChild();
    mDetail.mRenderable = Art.refdot;
    mDetail.mClipDistance = 0.5;

    mRNG = new Math.Random(17 + 37 * mBiomeX + 37 * mZ);

    if (mBiomeX == 0 && mBiomeZ == 0)
    {
      mBiomeType = 0;
    }
    else
    {
      if (mRNG.nextDouble() > 0.8)
      {
        mBiomeType = mRNG.nextInt(world.mGenerators.length-1) + 1;
      }
      else
      {
        mBiomeType = 1;
      }
    }

    world.mGenerators[mBiomeType].generate(this);
  }

  bool getIs(int x, int z)
  {
    return mX == x && mZ == z;
  }

  void setEnabled(bool enabled)
  {
    mEnabled = enabled;
    mNode.mVisible = mEnabled;
  }

  int distance(CChunk other)
  {
    double hx = (mX - other.mX).toDouble();
    double hz = (mZ - other.mZ).toDouble();
    double h = Math.sqrt(hx * hx + hz * hz);
    return h.ceil();
  }

}

///////////////////////////////////////////////
