/**

    Manta

    Copyright (c) 2013 Robin Southern, https://bitbucket.org/betajaen/manta

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

*/

part of manta;

///////////////////////////////////////////////

class CAircraftDesign
{
  double mThrustMax = 15000.0;
  double mSpeedMax = 140.0;
  double mMass = 3200.0;
  double mInvMass = 1.0 / 3200.0;
  double mAngleOfAttackMax = 20.0 * degrees2radians;
  double mAngleOfAttackLimit = 0.25;
  double mAngleOfAttackDegree = 60.0 * degrees2radians;
  double mRollRate = 5.0 * degrees2radians;
  double mPitchRate = 1.5 * degrees2radians;
  double mYawRate = 0.1 * degrees2radians;
  double mLiftOfSpeed = 45.0;
  double mLiftRangeFactor = 1.75;
  double mLiftRangeFactorSpeed = 1.25;
  double mLiftRangeFactorAofA = 2.00;
}

CAircraftDesign aircraftDesign = new CAircraftDesign();

///////////////////////////////////////////////

abstract class CAircraftController
{
  CAircraft mAircraft;

  CAircraftController(CAircraft aircraft)
  {
    mAircraft = aircraft;
  }

  void tick(double dt);

}

///////////////////////////////////////////////

class CPlayerAircraftController extends CAircraftController
{

  CPlayerAircraftController(CAircraft aircraft) : super(aircraft)
  {
  }

  void tick(double dt)
  {
    if (Game.isKeyDown(KeyCode.EQUALS))
    {
      mAircraft.mThrottle = CUtil.clamp01(mAircraft.mThrottle + 1.0 * dt);
    }
    else if (Game.isKeyDown(KeyCode.DASH))
    {
      mAircraft.mThrottle = CUtil.clamp01(mAircraft.mThrottle - 1.0 * dt);
    }

    if (Game.isKeyDown(KeyCode.SHIFT))
    {
      mAircraft.mBrakes = CUtil.clamp01(mAircraft.mBrakes + 5.0 * dt);
    }
    else
    {
      mAircraft.mBrakes = CUtil.toZero(mAircraft.mBrakes, 0.5, dt);
    }

    if (Game.isKeyDown(KeyCode.LEFT))
    {
      mAircraft.mRollSurface = CUtil.clampn11(mAircraft.mRollSurface + 1.0 * dt);
    }
    else if (Game.isKeyDown(KeyCode.RIGHT))
    {
      mAircraft.mRollSurface = CUtil.clampn11(mAircraft.mRollSurface - 1.0 * dt);
    }
    else
    {
        mAircraft.mRollSurface = CUtil.toZero(mAircraft.mRollSurface, 5.0, dt);
    }

    if (Game.isKeyDown(KeyCode.UP))
    {
      mAircraft.mPitchSurface = CUtil.clampn11(mAircraft.mPitchSurface + 1.0 * dt);
    }
    else if (Game.isKeyDown(KeyCode.DOWN))
    {
      mAircraft.mPitchSurface = CUtil.clampn11(mAircraft.mPitchSurface - 1.0 * dt);
    }
    else
    {
      mAircraft.mPitchSurface = CUtil.toZero(mAircraft.mPitchSurface, 5.0, dt);
    }
  }

}

///////////////////////////////////////////////

class CAIAircraftController extends CAircraftController
{
  CAIAircraftController(CAircraft aircraft) : super(aircraft)
  {
  }

  void tick(double dt)
  {
     // ...
  }
}

///////////////////////////////////////////////

class CAircraftPhysics
{

  static Vector3 sForward = new Vector3(0.0,  0.0, -1.0);
  static Vector3 sUp      = new Vector3(0.0,  1.0,  0.0);
  static Vector3 sRight   = new Vector3(1.0,  0.0,  0.0);
  static const double s2Pi = Math.PI * 2.0;    // 360
  static const double sHalfPi = Math.PI * 0.5; // 90
  static Quaternion sYawQuat = new Quaternion.identity();
  static Quaternion sRollQuat = new Quaternion.identity();
  static Quaternion sPitchQuat = new Quaternion.identity();

  static const double sGravityAcceleration = 10.0;
  static Vector3 sGravity = new Vector3(0.0, -sGravityAcceleration, 0.0);

  CAircraft mAircraft;
  double    mRoll, mRollChange;
  double    mPitch, mPitchChange;
  double    mYaw, mYawChange;
  double    mThrust;
  double    mDrag;
  double    mLift;
  double    mSpeed;
  double    mAngleOfAttack;

  Vector3   mThrustAcceleration;
  Vector3   mDragAcceleration;
  Vector3   mLiftAcceleration;
  Vector3   mAcceleration;
  Vector3   mVelocity;

  bool      mIsFlying;
  bool      mIsMoving;
  bool      mIsMovingForward;

CAircraftPhysics(CAircraft aircraft)
  {
    mAircraft = aircraft;
    mRoll = 0.0;
    mPitch = 0.0;
    mYaw = 0.0;
    mThrust = 0.0;
    mDrag = 0.0;
    mLift = 0.0;
    mAngleOfAttack = 0.0;
    mThrustAcceleration = new Vector3.zero();
    mDragAcceleration = new Vector3.zero();
    mLiftAcceleration = new Vector3.zero();
    mAcceleration = new Vector3.zero();
    mVelocity = new Vector3.zero();
  }

  void tick(double dt)
  {
    // Calculate frame-by-frame properties, based upon the previous frame.
    mIsFlying = mAircraft.mPosition.y > 0.5;
    mSpeed = mVelocity.length;
    mIsMoving = mSpeed > 0.001;
    mIsMovingForward = false;

    if (mSpeed < 0.01)
    {
      mAcceleration.setZero();
      mVelocity.setZero();
      mDragAcceleration.setZero();
      mLiftAcceleration.setZero();
      mThrustAcceleration.setZero();
    }

    // Calculate changes of rolling, yawing, and pitching from control surfaces
    mRollChange  = mAircraft.mRollSurface  * aircraftDesign.mRollRate;
    mYawChange   = mAircraft.mYawSurface  * aircraftDesign.mYawRate;
    mPitchChange = mAircraft.mPitchSurface  * aircraftDesign.mPitchRate;

    // Add controlled rolling, yawing and pitching
    mRoll  += mRollChange;
    mYaw   += mYawChange;// + mRollChange * 1.05;
    mPitch += mPitchChange;// - mRollChange * 1.05;

    // Normalise angles
    mRoll = mRoll % s2Pi;
    mYaw = mYaw % s2Pi;
    mPitch = mPitch % s2Pi;

    // Limit angles when on ground
    if (mIsFlying == false)
    {
      // No rolling, as the wings will phase through into the ground.
      mRoll = 0.0;

      // No moving, then no pitching.
      if (!mIsMoving)
      {
        mPitch = s2Pi;
      }

      // When moving forward only pitch upwards, never down into the ground.
      if (mIsMoving && mPitch < Math.PI)
      {
          mPitch = s2Pi;
      }
    }

    // Orientate
    sYawQuat.setAxisAngle(sUp, mYaw);
    sPitchQuat.setAxisAngle(sRight, mPitch);
    sRollQuat.setAxisAngle(sForward, mRoll);

    mAircraft.mOrientation = new Quaternion.identity();
    mAircraft.mOrientation *= sRollQuat;
    mAircraft.mOrientation *= sPitchQuat;
    mAircraft.mOrientation *= sYawQuat;

    // Calculate relative axes
    mAircraft.mForward.setFrom(sForward);
    mAircraft.mOrientation.rotate(mAircraft.mForward);

    mAircraft.mUp.setFrom(sUp);
    mAircraft.mOrientation.rotate(mAircraft.mUp);

    mAircraft.mRight.setFrom(sRight);
    mAircraft.mOrientation.rotate(mAircraft.mRight);

    // Calculate Angle of Attack
    if (mIsMoving)
    {
      mAngleOfAttack = CUtil.angle(mAircraft.mUp, mVelocity) - sHalfPi;
    }
    else
    {
      mAngleOfAttack = 0.0;
    }

    // Calculate thrust
    mThrust = aircraftDesign.mThrustMax * mAircraft.mThrottle;
    mThrustAcceleration.setFrom(mAircraft.mForward);
    mThrustAcceleration.scale(mThrust * aircraftDesign.mInvMass);

    // Calculate drag
    if (mIsMoving)
    {
      mDrag = mSpeed / aircraftDesign.mSpeedMax;
      mDrag += Math.min( aircraftDesign.mAngleOfAttackLimit, mAngleOfAttack.abs() / aircraftDesign.mAngleOfAttackDegree);
      mDrag *= aircraftDesign.mThrustMax;
      mDragAcceleration.setFrom(mAircraft.mForward);
      mDragAcceleration.scale(-1.0);
      mDragAcceleration.scale(mDrag * aircraftDesign.mInvMass);
    }

    // Calculate Lift
    if (mIsMoving)
    {
      double ls = Math.min(1.5, mSpeed / aircraftDesign.mLiftOfSpeed);
      ls = CUtil.clamp(ls, -aircraftDesign.mLiftRangeFactorSpeed, aircraftDesign.mLiftRangeFactorSpeed);

      double laoa = mAngleOfAttack / aircraftDesign.mAngleOfAttackMax;
      laoa = CUtil.clamp(laoa, -aircraftDesign.mLiftRangeFactorAofA, aircraftDesign.mLiftRangeFactorAofA);

      double lf = ls * (1.0 + laoa);
      lf = CUtil.clamp(lf, -aircraftDesign.mLiftRangeFactor, aircraftDesign.mLiftRangeFactor);

      mLift = lf * sGravityAcceleration;
      mLiftAcceleration = mAircraft.mUp * mLift;
    }

    // Add up acceleration
    mAcceleration = mThrustAcceleration + mDragAcceleration + mLiftAcceleration + sGravity;

    // Add to velocity by time
    mVelocity += mAcceleration * dt;

    if (mIsFlying == false)
    {
      if (mVelocity.y < 0)
      {
        mVelocity.y = 0.0;
        mAcceleration.y = 0.0;
      }
    }

  }

}

///////////////////////////////////////////////

class CAircraft extends CNode
{

  CAircraftPhysics    mPhysics;
  CAircraftController mController;
  Quaternion          mOrientation;
  Vector3             mForward, mUp, mRight;
  double              mThrottle, mBrakes, mRollSurface, mPitchSurface, mYawSurface;
  int                 mAltitudeFeet, mSpeedMPH;

  CAircraft(bool isPlayer) : super()
  {
    mPhysics = new CAircraftPhysics(this);
    mController = (isPlayer ? new CPlayerAircraftController(this) : new CAIAircraftController(this));
    mForward = new Vector3(0.0, 0.0, -1.0);
    mUp = new Vector3(0.0, 1.0, 0.0);
    mRight = new Vector3(1.0, 0.0, 0.0);
    mThrottle = 0.0;
    mBrakes = 0.0;
    mRollSurface = 0.0;
    mPitchSurface = 0.0;
    mYawSurface = 0.0;
    mAltitudeFeet = 0;
  }

  void tick(double dt)
  {
    mController.tick(dt);
    mPhysics.tick(dt);
    mPosition += mPhysics.mVelocity * dt;
    if (mPosition.y < 0.5)
    {
      mPosition.y = 0.5;
    }
    mAltitudeFeet = ((mPosition.y  - 0.5) * 3.821).toInt();
    mSpeedMPH = (mPhysics.mSpeed * 2.237).toInt();
  }

}

///////////////////////////////////////////////
