/**

    Manta

    Copyright (c) 2013 Robin Southern, https://bitbucket.org/betajaen/manta

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

*/

part of manta;

///////////////////////////////////////////////

class CColours
{
  Map<String, Vector3> mColours;

  CColours()
  {
    mColours = new Map<String, Vector3>();

    add('Shadow', 0, 0, 0);

    add('AliceBlue',94,97,100);
    add('AntiqueWhite',98,92,84);
    add('Aqua',0,100,100);
    add('Aquamarine',50,100,83);
    add('Azure',94,100,100);
    add('Beige',96,96,86);
    add('Bisque',100,89,77);
    add('Black',1,1,1);
    add('BlanchedAlmond',100,92,80);
    add('Blue',0,0,100);
    add('BlueViolet',54,17,89);
    add('Brown',65,16,16);
    add('Burlywood',87,72,53);
    add('CadetBlue',37,62,63);
    add('Chartreuse',50,100,0);
    add('Chocolate',82,41,12);
    add('Coral',100,50,31);
    add('Cornflower',39,58,93);
    add('Cornsilk',100,97,86);
    add('Crimson',86,8,24);
    add('Cyan',0,100,100);
    add('DarkBlue',0,0,55);
    add('DarkCyan',0,55,55);
    add('DarkGoldenrod',72,53,4);
    add('DarkGray',66,66,66);
    add('DarkGreen',0,39,0);
    add('DarkKhaki',74,72,42);
    add('DarkMagenta',55,0,55);
    add('DarkOliveGreen',33,42,18);
    add('DarkOrange',100,55,0);
    add('DarkOrchid',60,20,80);
    add('DarkRed',55,0,0);
    add('DarkSalmon',91,59,48);
    add('DarkSeaGreen',56,74,56);
    add('DarkSlateBlue',28,24,55);
    add('DarkSlateGray',18,31,31);
    add('DarkTurquoise',0,81,82);
    add('DarkViolet',58,0,83);
    add('DeepPink',100,8,58);
    add('DeepSkyBlue',0,75,100);
    add('DimGray',41,41,41);
    add('DodgerBlue',12,56,100);
    add('Firebrick',70,13,13);
    add('FloralWhite',100,98,94);
    add('ForestGreen',13,55,13);
    add('Fuchsia',100,0,100);
    add('Gainsboro',86,86,86);
    add('GhostWhite',97,97,100);
    add('Gold',100,84,0);
    add('Goldenrod',85,65,13);
    add('Gray',50,50,50);
    add('Green',0,50,0);
    add('GreenYellow',68,100,18);
    add('Honeydew',94,100,94);
    add('HotPink',100,41,71);
    add('IndianRed',80,36,36);
    add('Indigo',29,0,51);
    add('Ivory',100,100,94);
    add('Khaki',94,90,55);
    add('Lavender',90,90,98);
    add('LavenderBlush',100,94,96);
    add('LawnGreen',49,99,0);
    add('LemonChiffon',100,98,80);
    add('LightBlue',68,85,90);
    add('LightCoral',94,50,50);
    add('LightCyan',88,100,100);
    add('LightGoldenrod',98,98,82);
    add('LightGray',83,83,83);
    add('LightGreen',56,93,56);
    add('LightPink',100,71,76);
    add('LightSalmon',100,63,48);
    add('LightSeaGreen',13,70,67);
    add('LightSkyBlue',53,81,98);
    add('LightSlateGray',47,53,60);
    add('LightSteelBlue',69,77,87);
    add('LightYellow',100,100,88);
    add('Lime',0,100,0);
    add('LimeGreen',20,80,20);
    add('Linen',98,94,90);
    add('Magenta',100,0,100);
    add('Maroon',50,0,0);
    add('MediumAquamarine',40,80,67);
    add('MediumBlue',0,0,80);
    add('MediumOrchid',73,33,83);
    add('MediumPurple',58,44,86);
    add('MediumSeaGreen',24,70,44);
    add('MediumSlateBlue',48,41,93);
    add('MediumSpringGreen',0,98,60);
    add('MediumTurquoise',28,82,80);
    add('MediumVioletRed',78,8,52);
    add('MidnightBlue',10,10,44);
    add('MintCream',96,100,98);
    add('MistyRose',100,89,88);
    add('Moccasin',100,89,71);
    add('NavajoWhite',100,87,68);
    add('Navy',0,0,50);
    add('OldLace',99,96,90);
    add('Olive',50,50,0);
    add('OliveDrab',42,56,14);
    add('Orange',100,65,0);
    add('OrangeRed',100,27,0);
    add('Orchid',85,44,84);
    add('PaleGoldenrod',93,91,67);
    add('PaleGreen',60,98,60);
    add('PaleTurquoise',69,93,93);
    add('PaleVioletRed',86,44,58);
    add('PapayaWhip',100,94,84);
    add('PeachPuff',100,85,73);
    add('Peru',80,52,25);
    add('Pink',100,75,80);
    add('Plum',87,63,87);
    add('PowderBlue',69,88,90);
    add('Purple',50,0,50);
    add('Red',100,0,0);
    add('RosyBrown',74,56,56);
    add('RoyalBlue',25,41,88);
    add('SaddleBrown',55,27,7);
    add('Salmon',98,50,45);
    add('SandyBrown',96,64,38);
    add('SeaGreen',18,55,34);
    add('Seashell',100,96,93);
    add('Sienna',63,32,18);
    add('Silver',75,75,75);
    add('SkyBlue',53,81,92);
    add('SlateBlue',42,35,80);
    add('SlateGray',44,50,56);
    add('Snow',100,98,98);
    add('SpringGreen',0,100,50);
    add('SteelBlue',27,51,71);
    add('Tan',82,71,55);
    add('Teal',0,50,50);
    add('Thistle',85,75,85);
    add('Tomato',100,39,28);
    add('Turquoise',25,88,82);
    add('Violet',93,51,93);
    add('Wheat',96,87,70);
    add('White',100,100,100);
    add('WhiteSmoke',96,96,96);
    add('Yellow',100,100,0);
    add('YellowGreen',60,80,20);
  }

  void add(String name, int rPct, int gPct, int bPct)
  {
    double r = rPct.toDouble() * 0.01;
    double g = gPct.toDouble() * 0.01;
    double b = bPct.toDouble() * 0.01;

    mColours[name.toLowerCase()] = new Vector3(r, g, b);
  }

  Vector3 get(String name)
  {
    String ln = name.toLowerCase();
    if (mColours.containsKey(ln))
    {
      return mColours[ln];
    }
    return new Vector3(1.0, 1.0, 1.0);
  }

  Vector4 get4(String name)
  {
    Vector3 col = Colours.get(name);
    return new Vector4(col.x, col.y, col.z, 1.0);
  }
}

CColours Colours = new CColours();

///////////////////////////////////////////////
