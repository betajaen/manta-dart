/**

    Manta

    Copyright (c) 2013 Robin Southern, https://bitbucket.org/betajaen/manta

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

*/

part of manta;

///////////////////////////////////////////////

class CFlightState extends CState
{

  static const double     mInvYOffsetHeight = 1 / 300.0;

  CTransform mTransform;
  CNode      mRoot, mRunway, mPlane;
  CAircraft  mPlayer;
  CWorld     mWorld;

  CFlightState()
  {
    print("Doing Flight");

    mTransform = new CTransform();
    mTransform.setProjection3D();

    mWorld = new CWorld(mTransform);
    mRoot = mWorld.mRootNode;

    for (int i=0;i < 10;i++)
    {
      mRunway = mRoot.addChild();
      mRunway.mRenderable = Art.runway;
      mRunway.mPosition.setValues(0.0, 0.02, -400.0 - (i.toDouble() * 400.0));
      mRunway.mRotation.setValues(0.0, 90.0, 0.0);
    }

    mPlayer = mRoot.addUserChild(new CAircraft(true));
    mPlayer.mPosition.setValues(0.0, 0.5, 400.0);
    mTransform.setCameraLookAt(mPlayer.mPosition, mPlayer.mPosition + new Vector3(0.0, 0.0, -1.0));


  }

  void stop()
  {

  }

  void tick(double deltaTime)
  {
    gl.clearColor(0.4, 0.49, 0.7337, 1.0);
    gl.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

    mWorld.SetPlayer(mPlayer.mPosition);
    mPlayer.tick(deltaTime);
    mWorld.tick(deltaTime);

    mTransform.mCameraPosition = mPlayer.mPosition;
    mTransform.mCameraTarget = mPlayer.mPosition + mPlayer.mForward;
    mTransform.mCameraUp = mPlayer.mUp;
    mTransform.updateCamera();
    mTransform.mYOffset = CUtil.clamp01(mInvYOffsetHeight * (mPlayer.mPosition.y - 0.5));

    mTransform.setProjection3DTerrain();
    mRoot.drawSelf(CNode.kTerrain);
    gl.clear(GL.DEPTH_BUFFER_BIT);

    mTransform.setProjection3D();
    mWorld.mPlayerChunk.mNode.drawSelf(CNode.kTerrain);
    mRoot.drawSelf(CNode.kObjects);


    GUI.drawSmallText(320-80, 2, 0, " DT: ${(deltaTime).toStringAsFixed(8)}");
    GUI.drawSmallText(320-80, 12, 0, "NCH: ${(mWorld.mChunks.length)}");
    GUI.drawSmallText(320-80, 22, 0, "CHX: ${(mWorld.mPlayerX)}");
    GUI.drawSmallText(320-80, 32, 0, "CHZ: ${(mWorld.mPlayerZ)}");
    GUI.drawSmallText(320-80, 42, 0, "BIO: ${(mWorld.mPlayerChunk.mBiomeType)}");
    GUI.drawSmallText(320-80, 52, 0, "YOF: ${(mTransform.mYOffset).toStringAsFixed(4)}");

    GUI.drawSmallText(2, 2, 0, "ROL: ${(mPlayer.mPhysics.mRoll).toStringAsFixed(8)}");
    GUI.drawSmallText(2, 12, 0, "PIT: ${(mPlayer.mPhysics.mPitch).toStringAsFixed(8)}");
    GUI.drawSmallText(2, 22, 0, "YAW: ${(mPlayer.mPhysics.mYaw).toStringAsFixed(8)}");


    GUI.drawSmallText(2, 42, 0, "THR: ${mPlayer.mPhysics.mThrust.toStringAsFixed(2)}");
    GUI.drawSmallText(2, 52, 0, "DRG: ${mPlayer.mPhysics.mDrag.toStringAsFixed(2)}");
    GUI.drawSmallText(2, 62, 0, "LFT: ${mPlayer.mPhysics.mLift.toStringAsFixed(2)}");
    GUI.drawSmallText(2, 72, 0, "ALT: ${mPlayer.mAltitudeFeet}");
    GUI.drawSmallText(2, 82, 0, "SPD: ${mPlayer.mSpeedMPH}");
    GUI.drawSmallText(2, 92, 0, "AoA: ${mPlayer.mPhysics.mAngleOfAttack.toStringAsFixed(2)}");

    GUI.drawSmallText(2,112, 0, "VEX: ${mPlayer.mPhysics.mVelocity.x.toStringAsFixed(2)}");
    GUI.drawSmallText(2,122, 0, "VEY: ${mPlayer.mPhysics.mVelocity.y.toStringAsFixed(2)}");
    GUI.drawSmallText(2,132, 0, "VEZ: ${mPlayer.mPhysics.mVelocity.z.toStringAsFixed(2)}");
    GUI.drawSmallText(2,142, 0, "POX: ${mPlayer.mPosition.x.toStringAsFixed(2)}");
    GUI.drawSmallText(2,152, 0, "POY: ${mPlayer.mPosition.y.toStringAsFixed(2)}");
    GUI.drawSmallText(2,162, 0, "POZ: ${mPlayer.mPosition.z.toStringAsFixed(2)}");
    GUI.drawSmallText(2,172, 0, "MOV? ${mPlayer.mPhysics.mIsMoving ? 'YES' : 'NO'}");
    GUI.drawSmallText(2,182, 0, "FWD? ${mPlayer.mPhysics.mIsMovingForward ? 'YES' : 'NO'}");
    GUI.drawSmallText(2,192, 0, "FLY? ${mPlayer.mPhysics.mIsFlying ? 'YES' : 'NO'}");

//    GUI.drawSurroundTextMPH(20, 50, 0, mPlayer.mSpeedMPH);
//    GUI.drawSurroundTextMPH(20, 60, 0, (mPlayer.mThrottle * 100).toInt());
//    GUI.drawSurroundTextAngle(153, 4, 0, mPlayer.mHeading);
//    GUI.drawSurroundTextAltitude(300, 50, 0, mPlayer.mAltitude);
//    GUI.drawSurroundText(20, 80, 0, mPlayer.mIsMoving.toString());
//    GUI.drawSurroundText(20, 90, 0, mPlayer.mIsMovingForward.toString());
//    GUI.drawSmallText(40, 20, 0, "ALT :${mPlayer.mPosition.y}");
//    GUI.drawSmallText(40, 30, 0, "LIFT:${mPlayer.mLift}");
//    GUI.drawSmallText(40, 40, 0, "TRST:${mPlayer.mThrust}");
//    GUI.drawSmallText(40, 50, 0, "DRAG:${mPlayer.mDrag}");
//    GUI.drawSmallText(40, 70, 0, "SPD :${mPlayer.mSpeed}");
//    GUI.drawSmallText(40, 80, 0, "VL.X:${mPlayer.mVelocity.x}");
//    GUI.drawSmallText(40, 90, 0, "VL.Y:${mPlayer.mVelocity.y}");
//    GUI.drawSmallText(40,100, 0, "VL.Z:${mPlayer.mVelocity.z}");
//    GUI.drawSmallText(40,110, 0, "AOFA:${mPlayer.mAngleOfAttack}");


  }

}

///////////////////////////////////////////////
